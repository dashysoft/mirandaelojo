﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Miranda_ElOjo.Models.ViewModels
{
    public class CiudadanosViewModel
    {
        public int IdCiu { get; set; }

        public int DniCiu { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Nombre")]
        public string NomCiu { get; set; }

        [Required]
        [StringLength(50)]
        [EmailAddress]
        [Display(Name = "Email")]
        public string EmailCiu { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Telefono")]
        public string TelfCiu { get; set; }

        [Required]
        [Display(Name = "Fecha Nacimiento")]
        public DateTime FechCreacion { get; set; }
    }
}