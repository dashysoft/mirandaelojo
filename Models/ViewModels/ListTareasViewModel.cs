﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Miranda_ElOjo.Models.ViewModels
{
    public class ListTareasViewModel
    {
        public int IdTarea { get; set; }
        public int IdCiu { get; set; }
        public int Dia { get; set; }
        public string Descripcion { get; set; }
    }
}