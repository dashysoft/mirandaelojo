﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Miranda_ElOjo.Models.ViewModels
{
    public class ListDiasViewModel
    {
        public int IdDia { get; set; }
        public string Dia { get; set; }
        public string ColorBoton { get; set; }
    }
}