﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Miranda_ElOjo.Models.ViewModels
{
    public class TareasViewModel
    {
        public int IdTarea{ get; set; }
        public int IdCiu { get; set; }

 
        [StringLength(100)]
        [Display(Name = "Nombre")]
        public string NomCiu { get; set; }

        public int IdDia { get; set; }

        [StringLength(50)]
        [Display(Name = "Dia")]
        public string Dia { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Descripcion")]
        public string Descripcion { get; set; }
    }
}