﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Miranda_ElOjo.Models.ViewModels
{
    public class ListCiudadanosViewModel
    {
        public int IdCiu { get; set; }
        public int DniCiu { get; set; }
        public string NomCiu { get; set; }
        public string EmailCiu { get; set; }
        public string TelfCiu { get; set; }

    }
}