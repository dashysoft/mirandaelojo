﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Miranda_ElOjo.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class db_mirandaEntities : DbContext
    {
        public db_mirandaEntities()
            : base("name=db_mirandaEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ciudadanos> ciudadanos { get; set; }
        public virtual DbSet<tareas> tareas { get; set; }
        public virtual DbSet<v_tareas> v_tareas { get; set; }
        public virtual DbSet<Dias> Dias { get; set; }
        public virtual DbSet<v_Dias> v_Dias { get; set; }
    }
}
