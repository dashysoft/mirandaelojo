﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Miranda_ElOjo.Models;
using Miranda_ElOjo.Models.ViewModels;
using System.Data.SqlClient;

namespace Miranda_ElOjo.Controllers
{
    public class CiudadanosController : Controller
    {

        // GET: Ciudadanos
        public ActionResult Index()
        {
            List<ListCiudadanosViewModel> lst;
            using (db_mirandaEntities db = new db_mirandaEntities())
            {
                lst = (from d in db.ciudadanos
                           select new ListCiudadanosViewModel
                           {
                               IdCiu = d.IdCiu,
                               DniCiu = (int)d.DniCiu,
                               NomCiu = d.NomCiu,
                               EmailCiu = d.EmailCiu,
                               TelfCiu = d.TelfCiu
                           }).ToList();
            }
            return View(lst);
        }
        public ActionResult Nuevo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Nuevo(CiudadanosViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (db_mirandaEntities db = new db_mirandaEntities())
                    {
                        var oCiudadanos = new ciudadanos();
                        oCiudadanos.DniCiu = model.DniCiu;
                        oCiudadanos.NomCiu = model.NomCiu;
                        oCiudadanos.EmailCiu = model.EmailCiu;
                        oCiudadanos.TelfCiu = model.TelfCiu;

                        db.ciudadanos.Add(oCiudadanos);
                        db.SaveChanges();
                    }
                    return Redirect("~/Ciudadanos/");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public ActionResult Editar(int Id)
        {
            CiudadanosViewModel model = new CiudadanosViewModel();
            using (db_mirandaEntities db = new db_mirandaEntities())
            {
                var oCiudadanos = db.ciudadanos.Find(Id);
                model.DniCiu= (int)oCiudadanos.DniCiu;
                model.NomCiu = oCiudadanos.NomCiu;
                model.EmailCiu = oCiudadanos.EmailCiu;
                model.TelfCiu = oCiudadanos.TelfCiu;
                model.IdCiu = oCiudadanos.IdCiu;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Editar(CiudadanosViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (db_mirandaEntities db = new db_mirandaEntities())
                    {
                        var oCiudadanos = db.ciudadanos.Find(model.IdCiu);
                        oCiudadanos.DniCiu = model.DniCiu;
                        oCiudadanos.NomCiu = model.NomCiu;
                        oCiudadanos.EmailCiu = model.EmailCiu;
                        oCiudadanos.TelfCiu = model.TelfCiu;
                        
                        db.Entry(oCiudadanos).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return Redirect("~/Ciudadanos/");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult Eliminar(int Id)
        {
            CiudadanosViewModel model = new CiudadanosViewModel();
            using (db_mirandaEntities db = new db_mirandaEntities())
            {
                var oCiudadanos = db.ciudadanos.Find(Id);
                db.ciudadanos.Remove(oCiudadanos);
                db.SaveChanges();
            }
            return Redirect("~/Ciudadanos/");
        }
    }
}