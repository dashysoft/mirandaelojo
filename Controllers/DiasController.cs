﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Miranda_ElOjo.Models;
using Miranda_ElOjo.Models.ViewModels;
using System.Data.SqlClient;

namespace Miranda_ElOjo.Controllers
{
    public class DiasController : Controller
    {
        db_mirandaEntities context = new db_mirandaEntities();

        public ActionResult Index()
        {
            List<ListDiasViewModel> lst;
            using (db_mirandaEntities db = new db_mirandaEntities())
            {
                lst = (from d in db.v_Dias
                       select new ListDiasViewModel
                       {
                           IdDia = d.IdDia,
                           Dia = d.Dia,
                           ColorBoton = d.ColorBoton
                       }).ToList();
            }
            return View(lst);
        }

        public ActionResult tareas(int Id)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@IdDia", Id)
            };
            var data = context.Database.SqlQuery<v_tareas>("Select_tareas_X_Dia @IdDia", param).ToList();
            return View(data);
        }
    }
}
