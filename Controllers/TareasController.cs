﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Miranda_ElOjo.Models;
using Miranda_ElOjo.Models.ViewModels;
using System.Data.SqlClient;

namespace Miranda_ElOjo.Controllers
{
    public class TareasController : Controller
    {
        //Esto es del gringo
        db_mirandaEntities context = new db_mirandaEntities();

        // GET: Tareas
        public ActionResult Index(int Id)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@IdCiu", Id)
            };
            var data = context.Database.SqlQuery<v_tareas>("Select_tareas_Ciudadano @IdCiu", param).ToList();
            return View(data);
        }

        public ActionResult Editar(int Id)
        {
            TareasViewModel model = new TareasViewModel();
            using (db_mirandaEntities db = new db_mirandaEntities())
            {
                var oTareas = db.v_tareas.Find(Id);
                model.IdTarea = oTareas.IdTarea;
                model.IdCiu = (int)oTareas.IdCiu;
                model.NomCiu = oTareas.NomCiu;
                model.IdDia = oTareas.IdDia;
                model.Dia = oTareas.Dia;
                model.Descripcion = oTareas.Descripcion;
            }

            List<DropDiaViewModel> lst = null;
            using (Models.db_mirandaEntities db = new Models.db_mirandaEntities())
            {
                lst = (from d in db.Dias
                       select new DropDiaViewModel
                       {
                           IdDia = d.IdDia,
                           Dia = d.Dia
                       }).ToList();
            }

            List<SelectListItem> items = lst.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Dia.ToString(),
                    Value = d.IdDia.ToString(),
                    Selected = true
                };
            });

            ViewBag.items = items;

            return View(model);
        }

        [HttpPost]
        public ActionResult Editar(TareasViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (db_mirandaEntities db = new db_mirandaEntities())
                    {
                        var oTareas = db.tareas.Find(model.IdTarea);
                        //oTareas.Dia = model.IdDia;
                        oTareas.Descripcion = model.Descripcion;

                        db.Entry(oTareas).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return Redirect("~/Tareas/");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult Eliminar(int Id)
        {
            TareasViewModel model = new TareasViewModel();
            using (db_mirandaEntities db = new db_mirandaEntities())
            {
                var oTareas = db.tareas.Find(Id);
                db.tareas.Remove(oTareas);
                db.SaveChanges();
            }
            return Redirect("~/Ciudadanos/");
        }

        public ActionResult Nuevo(int Id)
        {
            TareasViewModel model = new TareasViewModel();
            using (db_mirandaEntities db = new db_mirandaEntities())
            {
                var oCiudadanos = db.ciudadanos.Find(Id);
                model.IdCiu = oCiudadanos.IdCiu;
            }

            List<DropDiaViewModel> lst = null;
            using (Models.db_mirandaEntities db = new Models.db_mirandaEntities())
            {
                lst = (from d in db.Dias
                       select new DropDiaViewModel
                       {
                           IdDia = d.IdDia,
                           Dia = d.Dia
                       }).ToList();
            }

            List<SelectListItem> items = lst.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Dia.ToString(),
                    Value = d.IdDia.ToString(),
                    Selected = true
                };
            });

            ViewBag.items = items;
            return View(model);
        }

        [HttpPost]
        public ActionResult Nuevo(TareasViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (db_mirandaEntities db = new db_mirandaEntities())
                    {
                        var oTareas = new tareas();
                        oTareas.IdCiu = model.IdCiu;
                        oTareas.Dia = model.IdDia;
                        oTareas.Descripcion = model.Descripcion;

                        db.tareas.Add(oTareas);
                        db.SaveChanges();
                    }
                    return Redirect("~/Ciudadanos/");
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
