﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Miranda_ElOjo.Models;
using Miranda_ElOjo.Models.ViewModels;
using System.Data.SqlClient;
namespace Miranda_ElOjo.Controllers
{
    public class HomeController : Controller
    {
        db_mirandaEntities context = new db_mirandaEntities();

        public ActionResult Index()
        {
            var data = context.Database.SqlQuery<v_tareas>("Select_tareas_Actual").ToList();
            return View(data);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}