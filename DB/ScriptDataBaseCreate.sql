/****** Object:  Database [db_miranda]    Script Date: 10/5/2021 08:51:44 ******/
CREATE DATABASE [db_miranda]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_miranda', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_miranda.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'db_miranda_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\db_miranda_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_miranda].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [db_miranda] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [db_miranda] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [db_miranda] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [db_miranda] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [db_miranda] SET ARITHABORT OFF 
GO

ALTER DATABASE [db_miranda] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [db_miranda] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [db_miranda] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [db_miranda] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [db_miranda] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [db_miranda] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [db_miranda] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [db_miranda] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [db_miranda] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [db_miranda] SET  DISABLE_BROKER 
GO

ALTER DATABASE [db_miranda] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [db_miranda] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [db_miranda] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [db_miranda] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [db_miranda] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [db_miranda] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [db_miranda] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [db_miranda] SET RECOVERY FULL 
GO

ALTER DATABASE [db_miranda] SET  MULTI_USER 
GO

ALTER DATABASE [db_miranda] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [db_miranda] SET DB_CHAINING OFF 
GO

ALTER DATABASE [db_miranda] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [db_miranda] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [db_miranda] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [db_miranda] SET QUERY_STORE = OFF
GO

ALTER DATABASE [db_miranda] SET  READ_WRITE 
GO


USE [db_miranda]
GO
CREATE TABLE [dbo].[ciudadanos](
	[IdCiu] [int] IDENTITY(1,1) NOT NULL,
	[DniCiu] [int] NULL,
	[NomCiu] [varchar](100) NULL,
	[EmailCiu] [varchar](50) NULL,
	[TelfCiu] [varchar](50) NULL,
	[FechCreacion] [datetime] NULL,
 CONSTRAINT [PK_ciudadanos] PRIMARY KEY CLUSTERED 
(
	[IdCiu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO



CREATE TABLE [dbo].[Dias](
	[IdDia] [int] NOT NULL,
	[Dia] [varchar](50) NULL,
 CONSTRAINT [PK_Dias] PRIMARY KEY CLUSTERED 
(
	[IdDia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[tareas](
	[IdTarea] [int] IDENTITY(1,1) NOT NULL,
	[IdCiu] [int] NULL,
	[Dia] [int] NULL,
	[Descripcion] [varchar](255) NULL,
 CONSTRAINT [PK_tareas] PRIMARY KEY CLUSTERED 
(
	[IdTarea] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE VIEW [dbo].[v_Dias]
AS
SELECT d.IdDia, d.Dia, COUNT(t.Dia) as Totaltareas, (CASE COUNT(t.Dia) WHEN 0 THEN 'bg-warning' ELSE 'bg-success' END) AS ColorBoton
from Dias d
LEFT JOIN tareas t on d.IdDia = t.Dia
Group by d.IdDia, d.Dia
GO

CREATE VIEW [dbo].[v_tareas]
AS
SELECT        t.IdTarea, c.IdCiu, c.NomCiu, d.IdDia, d.Dia, t.Descripcion
FROM            dbo.tareas AS t 
				INNER JOIN dbo.ciudadanos AS c ON t.IdCiu = c.IdCiu
				INNER JOIN dbo.dias d on t.dia = d.IdDia
GO



CREATE procedure dbo.Select_tareas_Actual

as BEGIN
	Select IdTarea, IdCiu, NomCiu, IdDia, Dia, Descripcion 
	from v_tareas
	where IdDia = DATEPART(weekday, GETDATE())
	order by IdTarea asc
END
GO

CREATE procedure [dbo].[Select_tareas_Ciudadano] 
( @IdCiu int
)
as BEGIN
	Select IdTarea, IdCiu, NomCiu, IdDia, Dia, Descripcion 
	from v_tareas
	where IdCiu = @IdCiu
	order by Dia asc
END
GO


CREATE procedure [dbo].[Select_tareas_X_Dia] 
( @IdDia int
)
as BEGIN
	Select IdTarea, IdCiu, NomCiu, IdDia, Dia, Descripcion 
	from v_tareas
	where IdDia = @IdDia
	order by IdTarea asc
END
GO

DELETE FROM Dias
INSERT INTO Dias VALUES (1, 'DOMINGO')
INSERT INTO Dias VALUES (2, 'LUNES')
INSERT INTO Dias VALUES (3, 'MARTES')
INSERT INTO Dias VALUES (4, 'MIERCOLES')
INSERT INTO Dias VALUES (5, 'JUEVES')
INSERT INTO Dias VALUES (6, 'VIERNES')
INSERT INTO Dias VALUES (7, 'SABADO')
GO


